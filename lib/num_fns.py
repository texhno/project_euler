import math
from functools import *

def isDivBy(num, denom):
    return num % denom == 0

def fib(i):
    if i == 1: return 1
    elif i == 2: return 2
    return fib(i - 1) + fib(i - 2)

def sieve(num):
    prime = [True for i in range(num + 1)]
    p = 2
    while p * p <= num:
        if prime[p] == True:
            for i in range(p * p, num + 1, p):
                prime[i] = False
        p += 1

    primes = []

    for p in range(2, num + 1):
        if prime[p]:
            primes.append(p)
    
    return primes

def prodList(nums):
    return reduce(lambda x, y: x * y, nums)
