def prodAdj(take, i, j, matrix):
    result = [0, 0, 0, 0]
    # On the x axis
    if j < len(matrix[i]) - take + 1:
        result[0] = matrix[i][j] * matrix[i][j + 1] * matrix[i][j + 2] * matrix[i][j + 3]

    # On the y axis
    if i < len(matrix) - take + 1:
        result[1] = matrix[i][j] * matrix[i + 1][j] * matrix[i + 2][j] * matrix[i + 3][j]

    # Diagonally to the right
    if i < len(matrix) - take + 1 and j < len(matrix[i]) - take + 1:
        result[2] = matrix[i][j] * matrix[i + 1][j + 1] * matrix[i + 2][j + 2] * matrix[i + 3][j + 3]

    # Diagonally to the left
    if i < len(matrix) - take + 1 and j > take - 1:
        result[3] = matrix[i][j] * matrix[i + 1][j - 1] * matrix[i + 2][j - 2] * matrix[i + 3][j - 3]

    return result