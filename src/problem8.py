import math

triplets = [(a, b, c) for a in range(1, 500) for b in range(1, 500) for c in range(1, 500) if math.pow(a, 2) + math.pow(b, 2) == math.pow(c, 2) and a + b + c == 1000]

print(triplets[0][0] * triplets[0][1] * triplets[0][2])
