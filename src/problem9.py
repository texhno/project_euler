import sys
sys.path.append("lib")
from num_fns import sieve

primes = [p for p in sieve(10000000) if p < 2000000]

print(sum(primes))
