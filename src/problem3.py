import math
import sys
sys.path.append("lib")
from num_fns import sieve, isDivBy

target = 600851475143

primes = sieve(math.floor(math.sqrt(target)))
primes.reverse()

for i in primes:
    if isDivBy(target, i):
        print(i)
        break
