import sys
sys.path.append("lib")
from num_fns import isDivBy, fib

sum = 0

for i in range(1, 1000000):
    curr = fib(i)
    if curr > 4000000: break
    if isDivBy(curr, 2): sum += curr

print(sum)
