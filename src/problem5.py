import math

lim = 100

sumSquares = 0
limSum = 0
for i in range(1, lim + 1):
    sumSquares += math.pow(i, 2)
    limSum += i

squaresSum = math.pow(limSum, 2)

print(sumSquares)
print(squaresSum)

print(squaresSum - sumSquares)
