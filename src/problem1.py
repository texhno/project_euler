import sys
sys.path.append("lib")
from num_fns import isDivBy

sum = 0

for i in range(1, 1000):
    if isDivBy(i, 3) or isDivBy(i, 5): sum += i

print(sum)
